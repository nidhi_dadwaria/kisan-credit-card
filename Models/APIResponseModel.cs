﻿using KCC_App.KCCModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.Models
{
    public class APIResponseModel
    {
        public bool status { get; set; }
        public object data { get; set; }
        public FinalData dataD { get; set; }
        public string error { get; set; }
    }
    public class BatchResponseModel
    {
        public bool status { get; set; }
        public object data { get; set; }
        public string error { get; set; }
    }
    public class APIResponseModelMaster
    {
        public bool status { get; set; }
        public object data { get; set; }
        public List<MasterDataType> dataD { get; set; }
        public string error { get; set; }
    }
    public class MasterDataType { 
        public string type { get; set; }
        public List<MasterDataTypeOption> options { get; set; }
    }
    public class MasterDataTypeOption
    {
        public string value { get; set; }
        public string code { get; set; }

    }
    public class FinalData
    {
       public string stateCode { get; set; }
        public string stateName { get; set; }
        public string districtCode { get; set; }
        public string districtName { get; set; }
        public string bankCode { get; set; }
        public string bankName { get; set; }
        public string batchAckId { get; set; }

        public string batchId { get; set; }
        public int status { get; set; }
        public string[] errors { get; set; }

        public string[] applicationNumbers { get; set; }
        public List<Application> applications { get; set; }
        public List<States> states { get; set; }
        public List<Districts> districts { get; set; }
        public List<Locations> locations { get; set; }

        //public List<Banks> banks { get; set; }

        public List<Branches> branches { get; set; }
        public List<Crops> crops { get; set; }
        public List<MasterDataType> masters { get; set; }
    }
}
