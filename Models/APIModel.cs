﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.Models
{
    public class APIModel
    {
        public string stateCode { get; set; }
        public string districtCode { get; set; }
        public string bankCode { get; set; }
        public string masterCode { get; set; }
        public string cropCode { get; set; }
        public string batchAckId { get; set; }

        public string batchId { get; set; }
        public string financialYear { get; set; }
        public string[] applicationNumbers { get; set; } 
    }
}
