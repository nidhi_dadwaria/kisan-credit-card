﻿using DocumentFormat.OpenXml.Office2010.ExcelAc;
using KCC_App.KCCModels;
using System.Collections.Generic;

namespace KCC_App.Models
{
    public class LocationPageModel
    {
        public string district { get; set; }
        public List<Locations> locations { get; set; }
    }
    public class DashboardPageModel
    {
        public List<BatchStatus> batchStatuses { get; set; }
        public List<ApplicationStatus> appStatuses { get; set; }
        //public BatchStatusModel batchStatusModel { get; set; }
        //public ApplicationStatusModel applicationStatusModel { get; set; }

    }
    public class BatchStatusModel
    {
        public int total { get; set; }
        public int pending { get; set; }
        public int processed { get; set; }
        public int discarded { get; set; }
        public int processing { get; set; }

    }
    public class ApplicationStatusModel
    {
        public int total { get; set; }
        public int pending { get; set; }
        public int processed { get; set; }
        public int discarded { get; set; }
        public int processing { get; set; }

    }
}
