﻿using KCC_App.KCCModels;
using System.Threading.Tasks;

namespace KCC_App.Interfaces
{
    public interface IServiceRepository
    {
        Task<Batch> FileUpload(UploadModel model);
        Task SyncStates();
        Task SyncDistricts();
        Task SyncBranches();
        Task SyncLocations(string districtCode);
        Task SyncMasterData(string masterCode);
        Task SyncCrops();
        Task DeleteApplication(System.Collections.Generic.List<string> id,string batchId,string batchAckId);
    }
}
