﻿using System.Threading.Tasks;

namespace KCC_App.Interfaces
{
    public interface IBackgroundJobs
    {
        Task RefreshBatchStatus(string batchAckId, string batchId);
        Task RefreshBatchAndApplicationStatus(string batchAckId, string batchId);

    }
}
