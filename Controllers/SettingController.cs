﻿using Core.Flash;
using Hangfire;
using KCC_App.Data;
using KCC_App.Helpers;
using KCC_App.KCCModels;
using KCC_App.Models;
using KCC_App.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.Controllers
{
    [Authentication]
    public class SettingController : ControllerBase
    {
        private readonly ILogger<SettingController> _logger;
        private KCCConfig cCConfig;
        private readonly IFlasher f;
        readonly ApplicationDbContext dbContext;
        private readonly IConfiguration _configuration;
        public SettingController(ILogger<SettingController> logger, IConfiguration configuration, ApplicationDbContext context, IFlasher f)
        {
            _logger = logger;
            this._configuration = configuration;
            this.dbContext = context;
            this.f = f;
            this.cCConfig = context.KCCConfig.AsNoTracking().Include(x=>x.EncryptKey).Include(x=>x.Mapping).FirstOrDefault();
        }
        [HttpGet]
        public IActionResult Manage()
        {
           
            dbContext.Entry(this.cCConfig).State = EntityState.Detached;
            return View(this.cCConfig);
        }

       
        [HttpPost]
        public IActionResult SaveConfig(KCCConfig data)
        {
            
            //dbContext.Remove()
            dbContext.Update(data);
            dbContext.SaveChanges();
            f.Flash(Types.Success, "Config updated");
            //Flash("Config update", FlashLevel.Success);
            //Flash.Instance.Success("Config", "Config saved");
            //FlashMessage.Warning("Your error message");
            //this.cCConfig = dbContext.KCCConfig.AsNoTracking().Include(x => x.EncryptKey).Include(x => x.Mapping).FirstOrDefault();
            return RedirectToAction("Manage");
        }
    }
}
