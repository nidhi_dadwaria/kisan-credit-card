﻿using Microsoft.AspNetCore.Authorization;
using KCC_App.Data;
using KCC_App.Helpers;
using KCC_App.KCCModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KCC_App.Models;

namespace KCC_App.Controllers
{
    [Route("base")]
    [Authentication]
    public class BaseController : Controller
    {
        [Route("{**catchAll}")]
        public ActionResult Get(string catchAll)
        {
            
            return View(catchAll);
        }
        
    }
}
