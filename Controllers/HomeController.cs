﻿using Hangfire;
using KCC_App.Data;
using KCC_App.KCCModels;
using KCC_App.Models;
using Microsoft.AspNetCore.Authentication;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace KCC_App.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;
        public HomeController( IConfiguration configuration, ApplicationDbContext context)
        {
            _configuration = configuration;
            _context = context;
        }
        [Authentication]
        public IActionResult Index()
        {
            ViewData["Title"] = "Home";

            //Send the dashboard data to bind.
            List<ApplicationStatus> appStatuses= _context.ApplicationStatuses.AsNoTracking().ToList();
            appStatuses.Add(new ApplicationStatus { status = -1, Name = "Total", Count = 0 });
            
            List<BatchStatus> batchStatuses = _context.BatchStatuses.AsNoTracking().ToList();
            batchStatuses.Add(new BatchStatus { status = -1, Name = "Total", Count = 0 });


            DashboardPageModel model = new DashboardPageModel();
            //model.batchStatusModel = new BatchStatusModel();
            //model.applicationStatusModel = new ApplicationStatusModel();
            var data = _context.BatchResponseData.GroupBy(x=>x.BatchStatus.status).Select(g=> new { Key= g.Key, Count = g.Count() }).ToList();
            foreach(var item in batchStatuses)
            {
                item.Count = data.FirstOrDefault(x => x.Key == item.status)?.Count ?? 0;
            }
            var dataA = _context.Applications.GroupBy(x => x.AppStatus.status).Select(g => new { Key = g.Key, Count = g.Count() }).ToList();
            foreach (var item in appStatuses)
            {
                item.Count = dataA.FirstOrDefault(x => x.Key == item.status)?.Count ?? 0;
            }
            model.batchStatuses = batchStatuses.OrderBy(x => x.status).ToList();
            model.appStatuses = appStatuses.OrderBy(x=>x.status).ToList();
            return View(model);
            //return RedirectToActionPermanent("Login"); 
        }


        //public IActionResult Login()
        //{
        //    HttpContext.Session.Clear();
        //    return View();
        //}
        //public async Task<IActionResult> Login(loginModel model)
        //{
        //}
        //    public IActionResult Logout()
        //{
        //    HttpContext.Session.Clear();
        //    return RedirectToAction("Login");
        //}

        //[Authentication]
        //public IActionResult Privacy()
        //{
        //    return View();
        //}
        //[Authentication]
        //public IActionResult KCC()
        //{
        //    //string jobId = BackgroundJob.Enqueue<BackgroundJobs>(x => x.KCCUpload());
        //    return View();
        //}

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //[Authentication]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = System.Diagnostics.Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}

        //[HttpGet]
        //[Authentication]
        //public ActionResult DownloadSample()
        //{
        //    //string _filePath = Path.Combine(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory),"Formats","kcc.xls");
        //    //var fs = System.IO.File.OpenRead(_filePath);
        //    //return File(fs, "application/vnd.ms-excel", "kcc.xls");
        //    return View();
        //}
        [HttpGet]
        public IActionResult Login()
        {
            if (HttpContext.Session.GetString(Constants.KEY_USERNAME) == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public IActionResult Login(LoginModel model)
        {

            if (ModelState.IsValid)
            {
                var user = _context.LoginModels.FirstOrDefault(u => u.Username == model.Username && u.Password == model.Password);
                if (user != null)
                {

                    SetSessionForfurtherUsage(user.Username);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password");
                }
            }
            return View(model);
        }
        [Authentication]
        public IActionResult LogOut(LoginModel model)
        {
            ClearSession();
            return RedirectToAction("login", "Home");
        }
        private void ClearSession()
        {
            HttpContext.Session.Clear();
            HttpContext.Session.Remove(Constants.KEY_USERNAME);
            HttpContext.Session.Remove(Constants.KEY_STATECODE);
            HttpContext.Session.Remove(Constants.KEY_BANKCODE);
            HttpContext.Session.Remove(Constants.KEY_FINANCIALYEAR);
        }
        public void SetSessionForfurtherUsage(string username)
        {
            var cCConfig = _context.KCCConfig.AsNoTracking().FirstOrDefault();
            HttpContext.Session.SetString(Constants.KEY_USERNAME, username);
            HttpContext.Session.SetString(Constants.KEY_STATECODE,cCConfig.StateCode);
            HttpContext.Session.SetString(Constants.KEY_BANKCODE, cCConfig.BankCode);
            HttpContext.Session.SetString(Constants.KEY_FINANCIALYEAR, cCConfig.FinancialYear);
        }

    }
}
