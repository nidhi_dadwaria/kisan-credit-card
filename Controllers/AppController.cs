﻿using Core.Flash;
using DocumentFormat.OpenXml.InkML;
using KCC_App.Data;
using KCC_App.Helpers;
using KCC_App.KCCModels;
using KCC_App.Models;
using KCC_App.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.Controllers
{
    [Authentication]
    public class AppController : ControllerBase
    {
        private readonly ILogger<SettingController> _logger;
        //private KCCConfig cCConfig;
        private readonly IFlasher f;
        readonly ApplicationDbContext dbContext;
        private readonly IConfiguration _configuration;
        public AppController(ILogger<SettingController> logger, IConfiguration configuration, ApplicationDbContext context, IFlasher f)
        {
            _logger = logger;
            this._configuration = configuration;
            this.dbContext = context;
            this.f = f;
            //this.cCConfig = context.KCCConfig.AsNoTracking().Include(x => x.EncryptKey).Include(x => x.Mapping).FirstOrDefault();
        }
        public IActionResult Setting()
        {

            var years = new List<SelectListItem>();
            int startYear = DateTime.Now.Year-3;
            int endYear = DateTime.Now.Year +5;
            for (var i = startYear; i <= endYear; i++)
            {
                string text = string.Concat(i.ToString(), "-", (i + 1).ToString());
                years.Add(new SelectListItem
                {
                    Text = text,
                    Value = text,
                    Selected = true
                });

            }
            ViewBag.years = new SelectList(years, "Text", "Value"); 
            KCCConfig cCConfig = this.dbContext.KCCConfig.AsNoTracking().Include(x => x.EncryptKey).Include(x => x.Mapping).FirstOrDefault();
            //dbContext.Entry(this.cCConfig).State = EntityState.Detached;
            return View(cCConfig);
        }
        [HttpPost]
        public IActionResult Setting(KCCConfig data)
        {
            //dbContext.Remove()
            dbContext.Update(data);
            dbContext.SaveChanges();
            HttpContext.Session.SetString("stateCode", data.StateCode);
            HttpContext.Session.SetString("bankCode", data.BankCode);
            HttpContext.Session.SetString("financialYear", data.FinancialYear);
            f.Flash(Types.Success, "Config updated");
            //Flash("Config update", FlashLevel.Success);
            //Flash.Instance.Success("Config", "Config saved");
            //FlashMessage.Warning("Your error message");
            //this.cCConfig = dbContext.KCCConfig.AsNoTracking().Include(x => x.EncryptKey).Include(x => x.Mapping).FirstOrDefault();
            return RedirectToAction("Setting");
        }
        
    }
}
