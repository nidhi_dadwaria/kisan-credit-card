﻿using KCC_App.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.Controllers
{
    [Route("buttons")]
    [Authentication]
    public class ButtonsController : Controller
    {
        [Route("{**catchAll}")]
        public ActionResult Get(string catchAll)
        {
            
            return View(catchAll);
        }
    }
}
