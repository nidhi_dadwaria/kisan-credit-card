﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using KCC_App.Data;
using System.Threading.Tasks;
using KCC_App.Helpers;
using KCC_App.Services;
using Microsoft.Extensions.Configuration;
using Core.Flash;
using ClosedXML.Excel;
using System.IO;
using System.Data;
using NPOI.SS.Formula.Functions;
using KCC_App.Utils;
using Microsoft.AspNetCore.Mvc.Rendering;
using KCC_App.Models;
using NPOI.Util;
using Microsoft.EntityFrameworkCore;
using KCC_App.Interfaces;

namespace KCC_App.Controllers
{
    //[Route("master")]
    [Authentication]
    public class MasterController : Controller
    {
        private readonly ApplicationDbContext dbContext;
        private readonly IConfiguration _configuration;
        private readonly IFlasher f;
        private readonly IServiceRepository serviceRepository;
        public MasterController(ApplicationDbContext context, IConfiguration configuration, IFlasher f,IServiceRepository serviceRepository)
        {
            dbContext = context;
            this._configuration = configuration;
            this.f = f;
            this.serviceRepository = serviceRepository;

        }
        [HttpGet]
        public async Task<IActionResult> SyncState()
        {
            //ServiceRepository serviceRepository = new ServiceRepository(this._configuration, this.dbContext);
            await serviceRepository.SyncStates();
            f.Flash(Types.Success, "Sync Requested");
            return RedirectToAction("States");
        }
        public async Task<IActionResult> SyncDistricts()
        {
            //ServiceRepository serviceRepository = new ServiceRepository(this._configuration, this.dbContext);
            await serviceRepository.SyncDistricts();
            f.Flash(Types.Success, "Sync Requested");
            return RedirectToAction("Districts");
        }
        public async Task<IActionResult> SyncBranches()
        {
            //ServiceRepository serviceRepository = new ServiceRepository(this._configuration, this.dbContext);
            await serviceRepository.SyncBranches();
            f.Flash(Types.Success, "Sync Requested");
            return RedirectToAction("Branches");
        }
        public async Task<IActionResult> SyncCrops()
        {
            //ServiceRepository backgroundJobs = new ServiceRepository(this._configuration, this.dbContext);
            await serviceRepository.SyncCrops();
            f.Flash(Types.Success, "Sync Requested");
            return RedirectToAction("Crops");
        }
        public async Task<IActionResult> SyncLocations(string district)
        {
            if (string.IsNullOrEmpty( district) != null)
            {
                //ServiceRepository serviceRepository = new ServiceRepository(this._configuration, this.dbContext);
                await serviceRepository.SyncLocations(district);
                f.Flash(Types.Success, "Sync Requested");
            }
            else
            {
                f.Flash(Types.Success, "District not selected");

            }
            return RedirectToAction("Locations",new {district=district });
        }
        
        public async Task<IActionResult> SyncMasterData()
        {
            //ServiceRepository serviceRepository = new ServiceRepository(this._configuration, this.dbContext);
            await serviceRepository.SyncMasterData(null);
            f.Flash(Types.Success, "Sync Requested");
            return RedirectToAction("GeneralMaster");
        }
        public IActionResult States()
        {
            var st = dbContext.States.ToList();

            return View(st);
        }
        [HttpGet]
        public IActionResult exportToExcelForStates()
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
            
                DataTable dt = Common.ToDataTable(dbContext.States.ToList());
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "States.xls");
                }
            }
        }
        [HttpGet]
        public IActionResult exportToExcelForDistrict()
        {
            using (XLWorkbook wb = new XLWorkbook())
            {

                DataTable dt = Common.ToDataTable(dbContext.Districts.ToList());
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Districts.xls");
                }
            }
        }
        [HttpGet]
        public IActionResult exportToExcelForLocation()
        {
            using (XLWorkbook wb = new XLWorkbook())
            {

                DataTable dt = Common.ToDataTable(dbContext.Locations.ToList());
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Locations.xls");
                }
            }
        }
        [HttpGet]
        public IActionResult exportToExcelForCrops()
        {
            using (XLWorkbook wb = new XLWorkbook())
            {

                DataTable dt = Common.ToDataTable(dbContext.Crops.ToList());
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Crops.xls");
                }
            }
        }
        [HttpGet]
        public IActionResult exportToExcelForGeneralMaster()
        {
            using (XLWorkbook wb = new XLWorkbook())
            {

                DataTable dt = Common.ToDataTable(dbContext.GeneralMasters.ToList());
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "GeneralMasters.xls");
                }
            }
        }
        [HttpGet]
        public IActionResult exportToExcelForBranches()
        {
            using (XLWorkbook wb = new XLWorkbook())
            {

                DataTable dt = Common.ToDataTable(dbContext.Branches.ToList());
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Branches.xls");
                }
            }
        }
        public IActionResult Districts()
        {
            var dt = dbContext.Districts.ToList();

            return View(dt);

        }
        public IActionResult Branches()
        {
            var br = dbContext.Branches.ToList();

            return View(br);
        }

        public IActionResult Crops()
        {
            var cr = dbContext.Crops.ToList();

            return View(cr);
        }

        public IActionResult Locations(string district="")
        {
            ViewBag.districts = new SelectList(dbContext.Districts.ToList(), "districtCode", "districtName");
            if (string.IsNullOrEmpty(district))
            {
                district = ((SelectList)ViewBag.districts).ToList()[0].Value;
            }
            var lc = dbContext.Locations.Include(x => x.Districts).Where(x=>x.districtCode==district || district=="").ToList();
            LocationPageModel model = new LocationPageModel
            {
                district = district,
                locations = lc
            };
            return View(model);
        }

        public IActionResult GeneralMaster()
        {
            var gm = dbContext.GeneralMasters.OrderBy(x=>x.type).ToList();

            return View(gm);
        }
    }

}