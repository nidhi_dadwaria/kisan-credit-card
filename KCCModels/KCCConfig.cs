﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.KCCModels
{
    public class EncryptKey
    {
        [Key]
        public long EncryptKeyId { get; set; }
        public string IVkey { get; set; }
        public string Secretkey { get; set; }
        public KCCConfig KCCConfig { get; set; }
    }
    public class Mapping
    {
        [Key]
        public long MappingId { get; set; }
        public string beneficiaryName { get; set; }
        public string aadhaarNumber { get; set; }
        public string beneficiaryPassbookName { get; set; }
        public string mobile { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string socialCategory { get; set; }
        public string farmerCategory { get; set; }
        public string farmerType { get; set; }
        public string primaryOccupation { get; set; }
        public string relativeType { get; set; }
        public string relativeName { get; set; }

        public string residentialVillage { get; set; }
        public string residentialAddress { get; set; }
        public string residentialPincode { get; set; }

        public string accountNumber { get; set; }

        public string branchCode { get; set; }

        public string ifsc { get; set; }
        public string accountHolder { get; set; }

        public string joint_name { get; set; }

        public string joint_aadhaarNumber { get; set; }

        public string joint_isPrimary { get; set; }


        public string kccLoanSanctionedDate { get; set; }

        public string kccLimitSanctionedAmount { get; set; }

        public string kccDrawingLimitForFY { get; set; }

        public string activityType { get; set; }

        public string loanSanctionedDate { get; set; }

        public string loanSanctionedAmount { get; set; }

        public string landVillageName { get; set; }

        public string landVillage { get; set; }

        public string cropName { get; set; }

        public string cropCode { get; set; }

        public string plantationCode { get; set; }

        public string plantationArea { get; set; }

        public string liveStockType { get; set; }

        public string liveStockCode { get; set; }

        public string unitCount { get; set; }

        public string inlandType { get; set; }

        public string totalUnits { get; set; }

        public string totalArea { get; set; }

        public string marineType { get; set; }

        public string surveyNumber { get; set; }

        public string khataNumber { get; set; }

        public string landArea { get; set; }

        public string landType { get; set; }

        public string season { get; set; }
        public KCCConfig KCCConfig { get; set; }

    }
    public class KCCConfig
    {

        [Key]
        public long Id { get; set; }
        public string AuthKey { get; set; }
        public long EncryptKeyId { get; set; }
        public long MappingId { get; set; }
        public string BankCode { get; set; }
        public string StateCode { get; set; }
        public string EndPoint { get; set; }
        public string FinancialYear { get; set; }
        public string DataFilePath { get; set; }
        public EncryptKey EncryptKey { get; set; }
        public Mapping Mapping { get; set; }
    }
}
