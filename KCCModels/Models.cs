﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.KCCModels
{
    public class UploadModel
    {
        public DataTable dt { get; set; }
        public string batchId { get; set; }

        public string filePath { get; set; }

    }
}
