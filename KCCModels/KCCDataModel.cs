﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.KCCModels
{

    public class AccountDetails
    {
        [Key]
        public long Id { get; set; }
        public string accountNumber { get; set; }
        public string ifsc { get; set; }
        public string branchCode { get; set; }
        public int accountHolder { get; set; }
        public List<JointAccountHolder> jointAccountHolders { get; set; }
    }
    public class JointAccountHolder
    {
        [Key]
        public long Id { get; set; }
        public long AccountDetailId { get; set; }
        public string name { get; set; }
        public string aadhaarNumber { get; set; }
        public int isPrimary { get; set; }
        public AccountDetails AccountDetails { get; set; }
    }

    public class Activity
    {
        [Key]
        public long Id { get; set; }
        public int activityType { get; set; }
        public string loanSanctionedDate { get; set; }
        public int loanSanctionedAmount { get; set; }
        public List<ActivityRow> activityRows { get; set; }
    }

    public class ActivityRow
    {
        [Key]
        public long Id { get; set; }
        public long ActivityId { get; set; }
        public string landVillage { get; set; }
        public string cropCode { get; set; }
        public string surveyNumber { get; set; }
        public string khataNumber { get; set; }
        public double landArea { get; set; }
        public int landType { get; set; }
        public int season { get; set; }
        public string plantationCode { get; set; }
        public double? plantationArea { get; set; }
        public int? liveStockType { get; set; }
        public int? liveStockCode { get; set; }
        public int? unitCount { get; set; }
        public int? inlandType { get; set; }
        public int? totalUnits { get; set; }
        public double? totalArea { get; set; }
        public int? marineType { get; set; }
        public Activity Activity { get; set; }
    }

    public class Application
    {
        [Key]
        public string uniqueId { get; set; }
        public string batchId { get; set; }
        public int recordStatus { get; set; }
        public int applicationStatus { get; set; }
        public int isDeleted { get; set; }

        //public int applicationNumber { get; set; }

        //public int farmerId { get; set; }


        [NotMapped]
        [JsonProperty("errors")]
        public object errorsJ { get; set; }

        [JsonProperty("errorsD")]
        public string[] errors { get; set; }
        public BasicDetails basicDetails { get; set; }
        public ResidentialDetails residentialDetails { get; set; }
        public AccountDetails accountDetails { get; set; }
        public LoanDetails loanDetails { get; set; }
        public Batch Batch { get; set; }
        public List<Activity> activities { get; set; }
        
        [ForeignKey("applicationStatus")]
        public ApplicationStatus AppStatus { get; set; }

        public string applicationNumber { get; set; }

        public string farmerId { get; set; }

        public string recipientUniqueID { get; set; }
    }

    public class BasicDetails
    {
        [Key]
        public long Id { get; set; }
        public string ApplicationId { get; set; }
        public string beneficiaryName { get; set; }
        public string aadhaarNumber { get; set; }
        public string beneficiaryPassbookName { get; set; }
        public string mobile { get; set; }
        public string dob { get; set; }
        public int gender { get; set; }
        public int socialCategory { get; set; }
        public int farmerCategory { get; set; }
        public int farmerType { get; set; }
        public int primaryOccupation { get; set; }
        public int relativeType { get; set; }
        public string relativeName { get; set; }
        public Application Application { get; set; }
    }
    



    public class LoanDetails
    {
        [Key]
        public long Id { get; set; }
        public string ApplicationId { get; set; }
        public string kccLoanSanctionedDate { get; set; }
        public int kccLimitSanctionedAmount { get; set; }
        public int kccDrawingLimitForFY { get; set; }
        public Application Application { get; set; }
    }

    public class ResidentialDetails
    {
        [Key]
        public long Id { get; set; }
        public string ApplicationId { get; set; }
        public string residentialVillage { get; set; }
        public string residentialAddress { get; set; }
        public string residentialPincode { get; set; }
        public Application Application { get; set; }
    }

    public class Batch
    {   [Key]
        public string batchId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long RecId { get; set; }
        public DateTime? dateTime { get; set; }
        public string financialYear { get; set; }
        public string filepath { get; set; }
        public List<Application> applications { get; set; }
        public BatchResponse BatchResponse { get; set; }
        public BatchResponseData BatchResponseData { get; set; }

    }
    public class BatchStatus
    {
        [Key]
        public int? status { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public int Count { get; set; }
        //public BatchResponseData BatchResponseData { get; set; }
    }
    public class ApplicationStatus
    {
        [Key]
        public int? status { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public int Count { get; set; }
    }
    public class KCCBody
    {
        public string data { get; set; }
        public string authCode { get; set; }
    }
    public class BatchResponse
    {
        [Key]
        public long Id { get; set; }
        public bool status { get; set; }
        public string batchId { get; set; }
        public string error { get; set; }
        //public string data { get; set; }
        //public BatchResponseData data { get; set; }
        public Batch Batch { get; set; }

    }
    public class BatchResponseData
    {
        [Key]
        public long Id { get; set; }
        public string batchId { get; set; }
        public int status { get; set; }
        public string[] errors { get; set; }
        public string batchAckId { get; set; }
        public Batch Batch { get; set; }

        [ForeignKey("status")]
        public BatchStatus BatchStatus { get; set; }
       
    }
    //public enum datastatus
    //{
    //    Discarded = 0,
    //    Pending = 1,
    //    Processing = 2,
    //    Processed = 3
    //}
}
