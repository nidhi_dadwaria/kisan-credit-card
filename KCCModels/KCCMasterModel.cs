﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.KCCModels
{

    public class Crops
    {
        [Key]
        public long Id { get; set; }
        public string cropCode { get; set; }
        public string cropName { get; set; }
        public string categoryCode { get; set; }
        public string categoryName { get; set; }
    }
    public class Branches
    {
        [Key]
        public string branchCode { get; set; }
        public string branchName { get; set; }
        public string ifscCode { get; set; }
        public string bankCode { get; set; }
    }

    public class States
    {
        [Key]
        public string stateCode { get; set; }
        public string stateName { get; set; }

        public List<Districts> Districts { get; set; }
    }

    public class Districts
    {
        [Key]
        public string districtCode { get; set; }
        public string districtName { get; set; }
        public string stateCode { get; set; }
        public States States { get; set; }
        public List<Locations> Locations { get; set; }
    }

    public class Locations
    {
        [Key]
        public long Id { get; set; }
        public string level4 { get; set; }
        public string level4Code { get; set; }
        public string level4Name { get; set; }
        public string level7 { get; set; }
        public string level7Code { get; set; }
        public string level7Name { get; set; }
        public string stateCode { get; set; }
        public string districtCode { get; set; }
        [ForeignKey("stateCode")]
        public States States { get; set; }
        [ForeignKey("districtCode")]
        public Districts Districts { get; set; }
    }

    public class GeneralMasters
    {
        [Key]
        public long Id { get; set; }
        public string type { get; set; }
        public string code { get; set; }
        public string value { get; set; }
    }

}
