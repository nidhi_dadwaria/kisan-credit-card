using KCC_App.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.PostgreSql;
using Hangfire.Dashboard;
using KCC_App.Interfaces;
using KCC_App.Services;
using static Org.BouncyCastle.Math.EC.ECCurve;
using KCC_App.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace KCC_App
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDistributedMemoryCache(); // Required for session

            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = true;
                options.IdleTimeout = TimeSpan.FromMinutes(30);
            });
            services.AddControllersWithViews();
            services.AddSession();
            services.AddRazorPages();
            services.AddFlashes().AddMvc();
            services.AddTransient<IServiceRepository, ServiceRepository>();
            services.AddTransient<IBackgroundJobs, BackgroundJobs>();
            //services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection")));
            services.AddHangfire(x => x.UsePostgreSqlStorage(Configuration.GetConnectionString("DefaultConnection")));
            services.AddHangfireServer(options=> {
                options.WorkerCount = 1;
                options.SchedulePollingInterval=TimeSpan.FromMinutes(1);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization(); 
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=login}/{id?}");

                endpoints.MapRazorPages();
            });
            var appPath = Configuration.GetSection("appPath").Value??"/";
            app.UseHangfireDashboard("/jobs", new DashboardOptions
            {
                AppPath = appPath,
                Authorization = new[] {new HangfireAuthenticationFilter()},
                IgnoreAntiforgeryToken = true 
            });
            //RecurringJob.AddOrUpdate<BackgroundJobs>("RefreshBatchStatus", x=>x.RefreshBatchStatus(),Cron.Hourly());
        }
    }
}
