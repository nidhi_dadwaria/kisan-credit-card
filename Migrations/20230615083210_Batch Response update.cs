﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace KCC_App.Migrations
{
    public partial class BatchResponseupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "dataId",
                table: "BatchResponse",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BatchResponseData",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    batchId = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    errors = table.Column<string[]>(nullable: true),
                    batchAckId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BatchResponseData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BatchResponseData_Batches_batchId",
                        column: x => x.batchId,
                        principalTable: "Batches",
                        principalColumn: "batchId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BatchResponse_dataId",
                table: "BatchResponse",
                column: "dataId");

            migrationBuilder.CreateIndex(
                name: "IX_BatchResponseData_batchId",
                table: "BatchResponseData",
                column: "batchId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BatchResponse_BatchResponseData_dataId",
                table: "BatchResponse",
                column: "dataId",
                principalTable: "BatchResponseData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BatchResponse_BatchResponseData_dataId",
                table: "BatchResponse");

            migrationBuilder.DropTable(
                name: "BatchResponseData");

            migrationBuilder.DropIndex(
                name: "IX_BatchResponse_dataId",
                table: "BatchResponse");

            migrationBuilder.DropColumn(
                name: "dataId",
                table: "BatchResponse");
        }
    }
}
