﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class BatchResponseandDatarelationremoved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BatchResponse_BatchResponseData_dataId",
                table: "BatchResponse");

            migrationBuilder.DropIndex(
                name: "IX_BatchResponse_dataId",
                table: "BatchResponse");

            migrationBuilder.DropColumn(
                name: "dataId",
                table: "BatchResponse");

            migrationBuilder.AddColumn<string>(
                name: "data",
                table: "BatchResponse",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "data",
                table: "BatchResponse");

            migrationBuilder.AddColumn<long>(
                name: "dataId",
                table: "BatchResponse",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BatchResponse_dataId",
                table: "BatchResponse",
                column: "dataId");

            migrationBuilder.AddForeignKey(
                name: "FK_BatchResponse_BatchResponseData_dataId",
                table: "BatchResponse",
                column: "dataId",
                principalTable: "BatchResponseData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
