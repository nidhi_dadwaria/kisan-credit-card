﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace KCC_App.Migrations
{
    public partial class KCCMasterFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Crops",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    cropCode = table.Column<string>(nullable: true),
                    cropName = table.Column<string>(nullable: true),
                    categoryCode = table.Column<string>(nullable: true),
                    categoryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Crops", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeneralMasters",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    type = table.Column<string>(nullable: true),
                    code = table.Column<string>(nullable: true),
                    value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralMasters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "States",
                columns: table => new
                {
                    stateCode = table.Column<string>(nullable: false),
                    stateName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_States", x => x.stateCode);
                });

            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    districtCode = table.Column<string>(nullable: false),
                    districtName = table.Column<string>(nullable: true),
                    stateCode = table.Column<string>(nullable: true),
                    StatesstateCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.districtCode);
                    table.ForeignKey(
                        name: "FK_Districts_States_StatesstateCode",
                        column: x => x.StatesstateCode,
                        principalTable: "States",
                        principalColumn: "stateCode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    level4 = table.Column<string>(nullable: true),
                    level4Code = table.Column<string>(nullable: true),
                    level4Name = table.Column<string>(nullable: true),
                    level7 = table.Column<string>(nullable: true),
                    level7Code = table.Column<string>(nullable: true),
                    level7Name = table.Column<string>(nullable: true),
                    stateCode = table.Column<string>(nullable: true),
                    districtCode = table.Column<string>(nullable: true),
                    StatesstateCode = table.Column<string>(nullable: true),
                    DistrictsdistrictCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Locations_Districts_DistrictsdistrictCode",
                        column: x => x.DistrictsdistrictCode,
                        principalTable: "Districts",
                        principalColumn: "districtCode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Locations_States_StatesstateCode",
                        column: x => x.StatesstateCode,
                        principalTable: "States",
                        principalColumn: "stateCode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Districts_StatesstateCode",
                table: "Districts",
                column: "StatesstateCode");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_DistrictsdistrictCode",
                table: "Locations",
                column: "DistrictsdistrictCode");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_StatesstateCode",
                table: "Locations",
                column: "StatesstateCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Crops");

            migrationBuilder.DropTable(
                name: "GeneralMasters");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "Districts");

            migrationBuilder.DropTable(
                name: "States");
        }
    }
}
