﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class ApplicationStatusrelationchanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_BatchStatuses_applicationStatus",
                table: "Applications");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_ApplicationStatuses_applicationStatus",
                table: "Applications",
                column: "applicationStatus",
                principalTable: "ApplicationStatuses",
                principalColumn: "status",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_ApplicationStatuses_applicationStatus",
                table: "Applications");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_BatchStatuses_applicationStatus",
                table: "Applications",
                column: "applicationStatus",
                principalTable: "BatchStatuses",
                principalColumn: "status",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
