﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class Locationrelationupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Districts_DistrictsdistrictCode",
                table: "Locations");

            migrationBuilder.DropForeignKey(
                name: "FK_Locations_States_StatesstateCode",
                table: "Locations");

            migrationBuilder.DropIndex(
                name: "IX_Locations_DistrictsdistrictCode",
                table: "Locations");

            migrationBuilder.DropIndex(
                name: "IX_Locations_StatesstateCode",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "DistrictsdistrictCode",
                table: "Locations");

            migrationBuilder.DropColumn(
                name: "StatesstateCode",
                table: "Locations");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_districtCode",
                table: "Locations",
                column: "districtCode");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_stateCode",
                table: "Locations",
                column: "stateCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Districts_districtCode",
                table: "Locations",
                column: "districtCode",
                principalTable: "Districts",
                principalColumn: "districtCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_States_stateCode",
                table: "Locations",
                column: "stateCode",
                principalTable: "States",
                principalColumn: "stateCode",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Districts_districtCode",
                table: "Locations");

            migrationBuilder.DropForeignKey(
                name: "FK_Locations_States_stateCode",
                table: "Locations");

            migrationBuilder.DropIndex(
                name: "IX_Locations_districtCode",
                table: "Locations");

            migrationBuilder.DropIndex(
                name: "IX_Locations_stateCode",
                table: "Locations");

            migrationBuilder.AddColumn<string>(
                name: "DistrictsdistrictCode",
                table: "Locations",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StatesstateCode",
                table: "Locations",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Locations_DistrictsdistrictCode",
                table: "Locations",
                column: "DistrictsdistrictCode");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_StatesstateCode",
                table: "Locations",
                column: "StatesstateCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Districts_DistrictsdistrictCode",
                table: "Locations",
                column: "DistrictsdistrictCode",
                principalTable: "Districts",
                principalColumn: "districtCode",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_States_StatesstateCode",
                table: "Locations",
                column: "StatesstateCode",
                principalTable: "States",
                principalColumn: "stateCode",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
