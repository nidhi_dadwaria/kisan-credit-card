﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class batchStatusRelationinapplications3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Applications_applicationStatus",
                table: "Applications",
                column: "applicationStatus");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_BatchStatuses_applicationStatus",
                table: "Applications",
                column: "applicationStatus",
                principalTable: "BatchStatuses",
                principalColumn: "status",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_BatchStatuses_applicationStatus",
                table: "Applications");

            migrationBuilder.DropIndex(
                name: "IX_Applications_applicationStatus",
                table: "Applications");
        }
    }
}
