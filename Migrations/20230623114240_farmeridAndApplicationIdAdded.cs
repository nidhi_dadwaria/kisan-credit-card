﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class farmeridAndApplicationIdAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "applicationNumber",
                table: "Applications",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "farmerId",
                table: "Applications",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "applicationNumber",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "farmerId",
                table: "Applications");
        }
    }
}
