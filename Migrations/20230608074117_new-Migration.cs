﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class newMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_Activity_ActivityId",
                table: "Applications");

            migrationBuilder.DropForeignKey(
                name: "FK_Applications_ActivityRow_ActivityRowId",
                table: "Applications");

            migrationBuilder.DropIndex(
                name: "IX_Applications_ActivityId",
                table: "Applications");

            migrationBuilder.DropIndex(
                name: "IX_Applications_ActivityRowId",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "ActivityId",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "ActivityRowId",
                table: "Applications");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ActivityId",
                table: "Applications",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ActivityRowId",
                table: "Applications",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Applications_ActivityId",
                table: "Applications",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_ActivityRowId",
                table: "Applications",
                column: "ActivityRowId");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_Activity_ActivityId",
                table: "Applications",
                column: "ActivityId",
                principalTable: "Activity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_ActivityRow_ActivityRowId",
                table: "Applications",
                column: "ActivityRowId",
                principalTable: "ActivityRow",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
