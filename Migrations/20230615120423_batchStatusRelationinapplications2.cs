﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class batchStatusRelationinapplications2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_BatchStatuses_recordStatus",
                table: "Applications");

            migrationBuilder.DropIndex(
                name: "IX_Applications_recordStatus",
                table: "Applications");

            migrationBuilder.AddColumn<int>(
                name: "applicationStatus",
                table: "Applications",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string[]>(
                name: "errors",
                table: "Applications",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "applicationStatus",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "errors",
                table: "Applications");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_recordStatus",
                table: "Applications",
                column: "recordStatus");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_BatchStatuses_recordStatus",
                table: "Applications",
                column: "recordStatus",
                principalTable: "BatchStatuses",
                principalColumn: "status",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
