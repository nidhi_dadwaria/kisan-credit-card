﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class ConfigTableRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_KCCConfig_EncryptKeyId",
                table: "KCCConfig");

            migrationBuilder.DropIndex(
                name: "IX_KCCConfig_MappingId",
                table: "KCCConfig");

            migrationBuilder.CreateIndex(
                name: "IX_KCCConfig_EncryptKeyId",
                table: "KCCConfig",
                column: "EncryptKeyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_KCCConfig_MappingId",
                table: "KCCConfig",
                column: "MappingId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_KCCConfig_EncryptKeyId",
                table: "KCCConfig");

            migrationBuilder.DropIndex(
                name: "IX_KCCConfig_MappingId",
                table: "KCCConfig");

            migrationBuilder.CreateIndex(
                name: "IX_KCCConfig_EncryptKeyId",
                table: "KCCConfig",
                column: "EncryptKeyId");

            migrationBuilder.CreateIndex(
                name: "IX_KCCConfig_MappingId",
                table: "KCCConfig",
                column: "MappingId");
        }
    }
}
