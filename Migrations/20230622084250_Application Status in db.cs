﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace KCC_App.Migrations
{
    public partial class ApplicationStatusindb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ApplicationStatuses",
                columns: table => new
                {
                    status = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationStatuses", x => x.status);
                });

            migrationBuilder.InsertData(
                table: "ApplicationStatuses",
                columns: new[] { "status", "Name" },
                values: new object[,]
                {
                    { 0, "Draft" },
                    { 1, "Pending for approval(Submitted)" },
                    { 2, "Approved" },
                    { 3, "Rejected" },
                    { 4, "Deleted" },
                    { 5, "Review Required" }
                });

            migrationBuilder.InsertData(
                table: "BatchStatuses",
                columns: new[] { "status", "Name" },
                values: new object[,]
                {
                    { 0, "Discarded" },
                    { 1, "Pending for processing" },
                    { 2, "Processing" },
                    { 3, "Processed" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationStatuses");

            migrationBuilder.DeleteData(
                table: "BatchStatuses",
                keyColumn: "status",
                keyValue: 0);

            migrationBuilder.DeleteData(
                table: "BatchStatuses",
                keyColumn: "status",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "BatchStatuses",
                keyColumn: "status",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "BatchStatuses",
                keyColumn: "status",
                keyValue: 3);
        }
    }
}
