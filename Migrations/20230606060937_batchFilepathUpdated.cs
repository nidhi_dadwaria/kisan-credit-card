﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KCC_App.Migrations
{
    public partial class batchFilepathUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "filepath",
                table: "Batches",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "filepath",
                table: "Batches");
        }
    }
}
