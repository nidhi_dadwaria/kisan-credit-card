﻿using KCC_App.Data;
using KCC_App.Interfaces;
using KCC_App.KCCModels;
using KCC_App.Models;
using KCC_App.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace KCC_App.Services
{
    public class ServiceRepository : IServiceRepository
    {
        readonly KCCConfig cCConfig;
        readonly ApplicationDbContext dbContext;
        private readonly IConfiguration _configuration;
        public ServiceRepository(IConfiguration configuration, ApplicationDbContext context)
        {
            this._configuration = configuration;
            //this.cCConfig = Common.GetKCCConfig(this._configuration);// JsonConvert.DeserializeObject<KCCConfig>(configuration.GetSection("KCC").Value);
            this.dbContext = context;
            this.cCConfig = dbContext.KCCConfig.AsNoTracking().Include(x=>x.Mapping).Include(x => x.EncryptKey).FirstOrDefault();
        }

        public async Task<Batch> FileUpload(UploadModel model)
        {
            //cCConfig.Mapping = JsonFileReader.Read<Mapping>(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory) + "\\mapping.json");
            long lastAppSeq = Common.NextApplicationNoLong(model.batchId, dbContext);
            List<Application> applications = new List<Application>();

            foreach (DataRow dataRow in model.dt.Rows)
            {
                if (dataRow[cCConfig.Mapping.beneficiaryName] != DBNull.Value)
                {
                    List<Activity> activities = new List<Activity>();
                    List<ActivityRow> activityRow = new List<ActivityRow>();
                    #region activityrows

                    if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 1)
                    {

                        activityRow.Add(new ActivityRow
                        {
                            landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
                            cropCode = dataRow[cCConfig.Mapping.cropCode].ToString(),
                            surveyNumber = dataRow[cCConfig.Mapping.surveyNumber].ToString(),
                            khataNumber = dataRow[cCConfig.Mapping.khataNumber].ToString(),
                            landArea = Convert.ToDouble(dataRow[cCConfig.Mapping.landArea]),
                            landType = Convert.ToInt32(dataRow[cCConfig.Mapping.landType]),
                            season = Convert.ToInt32(dataRow[cCConfig.Mapping.season])
                        });
                    }
                    else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 2)
                    {
                        activityRow.Add(new ActivityRow
                        {
                            landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
                            plantationCode = dataRow[cCConfig.Mapping.plantationCode].ToString(),
                            surveyNumber = dataRow[cCConfig.Mapping.surveyNumber].ToString(),
                            khataNumber = dataRow[cCConfig.Mapping.khataNumber].ToString(),
                            plantationArea = Convert.ToDouble(dataRow[cCConfig.Mapping.plantationArea])

                        });
                    }
                    else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 3)
                    {
                        activityRow.Add(new ActivityRow
                        {
                            landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
                            liveStockType = Convert.ToInt32(dataRow[cCConfig.Mapping.liveStockType]),
                            liveStockCode = Convert.ToInt32(dataRow[cCConfig.Mapping.liveStockCode]),
                            unitCount = Convert.ToInt32(dataRow[cCConfig.Mapping.unitCount])
                        });
                    }
                    else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 4)
                    {
                        activityRow.Add(new ActivityRow
                        {
                            landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
                            inlandType = Convert.ToInt32(dataRow[cCConfig.Mapping.inlandType]),
                            totalUnits = Convert.ToInt32(dataRow[cCConfig.Mapping.totalUnits]),
                            totalArea = Convert.ToDouble(dataRow[cCConfig.Mapping.totalArea])
                        });
                    }
                    else if (Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]) == 5)
                    {
                        activityRow.Add(new ActivityRow
                        {
                            landVillage = dataRow[cCConfig.Mapping.landVillage].ToString(),
                            marineType = Convert.ToInt32(dataRow[cCConfig.Mapping.marineType]),
                            totalUnits = Convert.ToInt32(dataRow[cCConfig.Mapping.totalUnits])
                        });
                    }
                    #endregion
                    activities.Add(new Activity
                    {
                        activityType = Convert.ToInt32(dataRow[cCConfig.Mapping.activityType]),
                        loanSanctionedDate = Common.KCCDate(dataRow[cCConfig.Mapping.loanSanctionedDate].ToString()),
                        loanSanctionedAmount = Convert.ToInt32(dataRow[cCConfig.Mapping.loanSanctionedAmount]),
                        activityRows = activityRow
                    });
                    Application app = new Application
                    {
                        uniqueId = model.batchId + (lastAppSeq.ToString().PadLeft(5, '0')),
                        recordStatus = 1,
                        basicDetails = new BasicDetails
                        {
                            beneficiaryName = dataRow[cCConfig.Mapping.beneficiaryName].ToString(),
                            aadhaarNumber = dataRow[cCConfig.Mapping.aadhaarNumber].ToString(),
                            beneficiaryPassbookName = dataRow[cCConfig.Mapping.beneficiaryPassbookName].ToString(),
                            mobile = dataRow[cCConfig.Mapping.mobile].ToString(),
                            dob = Common.KCCDate(dataRow[cCConfig.Mapping.dob].ToString()),//Date Format yyyy-MM-dd
                            gender = Convert.ToInt32(dataRow[cCConfig.Mapping.gender]),
                            socialCategory = Convert.ToInt32(dataRow[cCConfig.Mapping.socialCategory]),
                            farmerCategory = Convert.ToInt32(dataRow[cCConfig.Mapping.farmerCategory]),
                            farmerType = Convert.ToInt32(dataRow[cCConfig.Mapping.farmerType]),
                            primaryOccupation = Convert.ToInt32(dataRow[cCConfig.Mapping.primaryOccupation]),
                            relativeType = Convert.ToInt32(dataRow[cCConfig.Mapping.relativeType]),
                            relativeName = dataRow[cCConfig.Mapping.relativeName].ToString()
                        },
                        residentialDetails = new ResidentialDetails
                        {
                            residentialVillage = dataRow[cCConfig.Mapping.residentialVillage].ToString(),
                            residentialAddress = dataRow[cCConfig.Mapping.residentialAddress].ToString(),
                            residentialPincode = dataRow[cCConfig.Mapping.residentialPincode].ToString(),

                        },
                        accountDetails = new AccountDetails
                        {
                            accountNumber = dataRow[cCConfig.Mapping.accountNumber].ToString(),
                            branchCode = dataRow[cCConfig.Mapping.branchCode].ToString(),
                            ifsc = dataRow[cCConfig.Mapping.ifsc].ToString(),
                            accountHolder = Convert.ToInt32(dataRow[cCConfig.Mapping.accountHolder]),
                            jointAccountHolders = new List<JointAccountHolder>()

                        },
                        loanDetails = new LoanDetails
                        {
                            kccLoanSanctionedDate = Common.KCCDate(dataRow[cCConfig.Mapping.kccLoanSanctionedDate].ToString()),
                            kccLimitSanctionedAmount = Convert.ToInt32(dataRow[cCConfig.Mapping.kccLimitSanctionedAmount]),
                            kccDrawingLimitForFY = Convert.ToInt32(dataRow[cCConfig.Mapping.kccDrawingLimitForFY])
                        }

                    };
                    if (app.accountDetails.accountHolder == 2)
                    {
                        app.accountDetails.jointAccountHolders.Add(new JointAccountHolder
                        {
                            name = dataRow[cCConfig.Mapping.joint_name].ToString(),
                            aadhaarNumber = dataRow[cCConfig.Mapping.joint_aadhaarNumber].ToString(),
                            isPrimary = Convert.ToInt32(dataRow[cCConfig.Mapping.joint_isPrimary])
                        });
                    }

                    applications.Add(app);
                    lastAppSeq++;
                }
            }
            Batch newBatch = new Batch
            {
                batchId = model.batchId,
                financialYear = cCConfig.FinancialYear,

                applications = applications

            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(newBatch), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };

            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint+Constants.API_UPLOAD_BATCH);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                BatchResponseModel batchReponseModel = JsonConvert.DeserializeObject<BatchResponseModel>(response.Content);
                newBatch.BatchResponse = new BatchResponse
                {
                    status = batchReponseModel.status,
                    error = batchReponseModel.error,
                    batchId = model.batchId

                };
                if (batchReponseModel.status)
                {
                    newBatch.BatchResponseData = JsonConvert.DeserializeObject<BatchResponseData>(Common.Decrypt((string)batchReponseModel.data, cCConfig.EncryptKey));
                }
                else
                {
                    newBatch.BatchResponseData = new BatchResponseData
                    {
                        batchId = model.batchId,
                        //status = 0
                    };

                }
            }
            catch (Exception ex)
            {
                newBatch.BatchResponse = new BatchResponse
                {
                    batchId = model.batchId,
                    status = false,
                    error = ex.Message
                };
                newBatch.BatchResponseData = new BatchResponseData
                {
                    batchId = model.batchId,
                    //status = 0
                };
            }

            newBatch.filepath = model.filePath;
            newBatch.dateTime = DateTime.Now;
            dbContext.Batches.Add(newBatch);
            dbContext.SaveChanges();
            return newBatch;
        }
        public async Task SyncStates()
        {
            APIModel stateModel = new APIModel
            {
                stateCode = "00"

            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(stateModel), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };
            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint+Constants.API_ROUTE_STATE);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                APIResponseModel responseModel = JsonConvert.DeserializeObject<APIResponseModel>(response.Content);
                if (responseModel.status)
                {
                    responseModel.dataD = JsonConvert.DeserializeObject<FinalData>(Common.Decrypt((string)responseModel.data, cCConfig.EncryptKey));

                    if (await dbContext.States.AnyAsync())
                    {
                        dbContext.States.RemoveRange(this.dbContext.States);
                    }
                    //Clear the table and insert again.
                    dbContext.States.AddRange(responseModel.dataD.states);
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public async Task SyncDistricts()
        {
            APIModel model = new APIModel
            {
                stateCode = cCConfig.StateCode ?? "09"

            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(model), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };
            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint + Constants.API_ROUTE_DISTRICT);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                APIResponseModel responseModel = JsonConvert.DeserializeObject<APIResponseModel>(response.Content);
                if (responseModel.status)
                {
                    responseModel.dataD = JsonConvert.DeserializeObject<FinalData>(Common.Decrypt((string)responseModel.data, cCConfig.EncryptKey));

                    if (await dbContext.Districts.AnyAsync())
                    {
                        dbContext.Districts.RemoveRange(this.dbContext.Districts);
                    }
                    //Clear the table and insert again.
                    dbContext.Districts.AddRange(responseModel.dataD.districts);
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public async Task SyncBranches()
        {
            APIModel model = new APIModel
            {
                bankCode = cCConfig.BankCode

            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(model, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };
            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint + Constants.API_ROUTE_BRANCH);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                APIResponseModel responseModel = JsonConvert.DeserializeObject<APIResponseModel>(response.Content);
                if (responseModel.status)
                {
                    responseModel.dataD = JsonConvert.DeserializeObject<FinalData>(Common.Decrypt((string)responseModel.data, cCConfig.EncryptKey));

                    if (await this.dbContext.Branches.AnyAsync())
                    {
                        //If any entry exits then Clear the table.
                        dbContext.Branches.RemoveRange(this.dbContext.Branches);
                    }
                    foreach (Branches branch in responseModel.dataD.branches)
                    {
                        branch.bankCode = responseModel.dataD.bankCode;
                    }
                    dbContext.Branches.AddRange(responseModel.dataD.branches);

                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public async Task SyncLocations(string districtCode)
        {
            APIModel model = new APIModel
            {
                districtCode = districtCode

            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(model, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };
            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint + Constants.API_ROUTE_LOCATION);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                APIResponseModel responseModel = JsonConvert.DeserializeObject<APIResponseModel>(response.Content);
                if (responseModel.status)
                {
                    responseModel.dataD = JsonConvert.DeserializeObject<FinalData>(Common.Decrypt((string)responseModel.data, cCConfig.EncryptKey));
                    if (await dbContext.Locations.AsNoTracking().AnyAsync(x => x.stateCode == responseModel.dataD.stateCode && x.districtCode == responseModel.dataD.districtCode))
                    {
                        dbContext.Locations.RemoveRange(dbContext.Locations.Where(x => x.stateCode == responseModel.dataD.stateCode && x.districtCode == responseModel.dataD.districtCode));

                    }
                    foreach (Locations data in responseModel.dataD.locations)
                    {
                        data.stateCode = responseModel.dataD.stateCode;
                        data.districtCode = responseModel.dataD.districtCode;
                    }
                    dbContext.Locations.AddRange(responseModel.dataD.locations);
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public async Task SyncMasterData(string masterCode)
        {
            APIModel model = new APIModel
            {
                masterCode = masterCode ?? "00"

            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(model, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };
            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint + Constants.API_ROUTE_CONFIG);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                APIResponseModelMaster responseModel = JsonConvert.DeserializeObject<APIResponseModelMaster>(response.Content);
                if (responseModel.status)
                {
                    responseModel.dataD = JsonConvert.DeserializeObject<List<MasterDataType>>(Common.Decrypt((string)responseModel.data, cCConfig.EncryptKey));
                    if (await dbContext.GeneralMasters.AnyAsync())
                    {
                        dbContext.GeneralMasters.RemoveRange(dbContext.GeneralMasters);

                    }
                    List<GeneralMasters> list = new();
                    foreach (MasterDataType data in responseModel.dataD)
                    {
                        foreach (MasterDataTypeOption option in data.options)
                        {
                            list.Add(new GeneralMasters
                            {
                                type = data.type,
                                code = option.code,
                                value = option.value
                            });
                        }
                    }
                    dbContext.GeneralMasters.AddRange(list);
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public async Task SyncCrops()
        {
            APIModel stateModel = new APIModel
            {
                cropCode = "00"

            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(stateModel, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };
            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint + Constants.API_ROUTE_CROP);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                APIResponseModel responseModel = JsonConvert.DeserializeObject<APIResponseModel>(response.Content);
                if (responseModel.status)
                {
                    responseModel.dataD = JsonConvert.DeserializeObject<FinalData>(Common.Decrypt((string)responseModel.data, cCConfig.EncryptKey));

                    if (await dbContext.Crops.AnyAsync())
                    {
                        dbContext.Crops.RemoveRange(this.dbContext.Crops);
                    }
                    //Clear the table and insert again.
                    dbContext.Crops.AddRange(responseModel.dataD.crops);
                    await dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public async Task DeleteApplication(List<string>ids,string batchId,string batchAckId)
        {
            APIModel model = new APIModel
            {
                batchId = batchAckId,
                financialYear = cCConfig.FinancialYear,
                applicationNumbers = ids.ToArray()
            };
            string edata = Common.Encrypt(JsonConvert.SerializeObject(model), cCConfig.EncryptKey);

            var body = new KCCBody
            {
                authCode = cCConfig.AuthKey,
                data = edata
            };
            var client = new RestClient();
            var request = new RestRequest(cCConfig.EndPoint + Constants.API_ROUTE_DELETE_APPS);
            request.AddHeader("Content-Type", "application/json");
            request.AddBody(JsonConvert.SerializeObject(body), "application/json");
            var response = await client.ExecutePostAsync(request);
            try
            {
                APIResponseModel responseModel = JsonConvert.DeserializeObject<APIResponseModel>(response.Content);
                if (responseModel.status)
                {
                    responseModel.dataD = JsonConvert.DeserializeObject<FinalData>(Common.Decrypt((string)responseModel.data, cCConfig.EncryptKey));
                    string[] errors = responseModel.dataD.errors;
                    if (errors==null || errors.Length == 0)
                    {
                        dbContext.Applications.Remove(dbContext.Applications.Find(ids));
                    }
                    await dbContext.SaveChangesAsync();
                }
                else
                {
                    throw new Exception(responseModel.error);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to delete the application: "+ex.Message, ex);
            }

        }
    }
}
